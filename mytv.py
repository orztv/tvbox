import requests
import os
from datetime import datetime

# 获取JSON数据
def fetch_json_data(url):
    try:
        response = requests.get(url)
        response.raise_for_status()  # 检查请求是否成功
        return response.json()
    except requests.HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'An error occurred: {err}')

# 解析并组织数据
def parse_and_organize_data(data):
    channels_info = {}
    for item in data:
        channel = item.get('channel', '未知频道')
        title = item.get('title', '未知标题')
        video_urls = item.get('videoUrl', [])
        
        if video_urls:  # Skip if video_urls is empty
            channels_info.setdefault(channel, []).extend(
                [f"{title},{video_url}" for video_url in video_urls]
            )
    return channels_info

# 写入到TXT文件，并统计title的数量
def write_to_file_and_count_titles(channels_info):
    unique_titles = set()
    for videos in channels_info.values():
        for video in videos:
            title = video.split(',')[0]
            unique_titles.add(title)

    # 修改文件路径，使其指向public文件夹下
    public_dir = 'public'
    os.makedirs(public_dir, exist_ok=True)  # 确保public文件夹存在
    filename = f"{public_dir}/mytv.txt"  # 文件名包含路径

    try:
        with open(filename, 'w', encoding='utf-8') as file:
            for channel, videos in channels_info.items():
                file.write(f"{channel},#genre#\n")
                for video in videos:
                    title, video_url = video.split(',')
                    if video_url:  # 如果video_url不为空
                        file.write(f"{video}\n")
                file.write("\n")
        print(f"导出完成。总共有 {len(unique_titles)} 个不同的标题。")
        # 更新index.html文件
        update_index_html_live('mytv.txt', public_dir)
    except Exception as err:
        print(f"写入文件时出错: {err}")

# 更新index.html文件的函数
def update_index_html_live(file_name, file_path):
    index_file_path = os.path.join(file_path, "box", "index.html")
    try:
        with open(index_file_path, "a", encoding='utf-8') as index_file:
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            before_url = '/'
            index_file.write(f"<p><a href=\"{before_url}{file_name}\">https://live.tv.cloudns.be{before_url}{file_name}</a> - Last updated: {timestamp}</br></p>\n")
    except IOError as e:
        print(f"File error: {e}")


# 主程序
def main():
    url = 'https://raw.githubusercontent.com/lizongying/my-tv/main/app/src/main/res/raw/channels.json'
    data = fetch_json_data(url)
    if data:
        channels_info = parse_and_organize_data(data)
        write_to_file_and_count_titles(channels_info)

if __name__ == '__main__':
    main()