import requests
import os
from urllib.parse import urlparse, unquote
from datetime import datetime

def update_index_html_live(file_name, file_path):
    try:
        with open("./public/box/index.html", "a", encoding='utf-8') as index_file:
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            before_url = '/'
            index_file.write(f"<p><a href=\"{before_url}{file_name}\">https://live.tv.cloudns.be{before_url}{file_name}</a></br>")
    except IOError as e:
        print(f"File error: {e}")

# Define the URLs list with title and suffix parameters
urls_with_titles_and_suffixes = [
    ("https://xn--z6ut02b.tvbox.xn--fiqs8s/", "zb_tvbox_cn_noindex", ".txt"),
    ("https://%E5%AE%89%E5%8D%93%E5%93%A5.com/ZB", "androidGe_noindex", ".txt"),
    ("https://4k.tvbox.xn--fiqs8s/", "4k_tvbox_cn_noindex", ".m3u")
]

# Ensure the 'public' directory exists
if not os.path.exists('public'):
    os.makedirs('public')

# Define string replacements as a list of tuples (old, new)
replacements = [
    ("更多接口关注公众号：TVBoxBox", ""),
    ("微信公众号【TVBoxBox】获取", "公众号"),
    ("📡公众号【安卓哥影视仓】", "公众号"),
    ("更多直播源，关注微信公众号“TVBox”", ""),
    ("安卓哥影视仓", "骚微分享"),
    ("盒子迷", "骚微分享"),
    ("TVBoxBox", "骚微分享"),

    # Add more replacements as needed
]

# Fetch the web page content, perform replacements, and save it with the title and suffix as the filename
for url, title, suffix in urls_with_titles_and_suffixes:
    try:
        # Parse the URL and decode the domain part if necessary
        parsed_url = urlparse(url)
        hostname = unquote(parsed_url.hostname)  # Decoding the percent-encoded hostname
        decoded_url = parsed_url._replace(netloc=hostname).geturl()

        # Make the HTTP request to the decoded URL
        response = requests.get(decoded_url)
        response.raise_for_status()  # Raises an HTTPError if the HTTP request returned an unsuccessful status code
        file_content = response.text

        # Perform string replacements
        for old, new in replacements:
            file_content = file_content.replace(old, new)

        # Use the title and suffix parameters as the filename
        filename = title + suffix
        full_path = os.path.join("public", filename)
        with open(full_path, "w", encoding="utf-8") as f:
            f.write(file_content)
        
        update_index_html_live(filename, title)
    except Exception as e:
        print(f"An error occurred: {e}")