import requests
import re
from datetime import datetime

def fetch_and_replace(url, old_string, new_string, chunk_size=1024):
    replaced_content = ''
    try:
        with requests.get(url, stream=True) as response:
            response.raise_for_status()
            for chunk in response.iter_content(chunk_size=chunk_size):
                # Use 'ignore' to skip undecodable bytes
                replaced_content += chunk.decode('utf-8', 'ignore')
    except requests.RequestException as e:
        return f"Request error: {e}"
    except re.error as e:
        return f"Regex error: {e}"
    
    replaced_content = re.sub(old_string, new_string, replaced_content)
    return replaced_content

def write_to_file(file_path, content):
    try:
        with open(file_path, "w", encoding='utf-8') as file:
            file.write(content)
    except IOError as e:
        print(f"File error: {e}")


def update_index_html(file_name, file_path):
    try:
        with open("./public/box/index.html", "a", encoding='utf-8') as index_file:
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            before_url = '/box/'
            index_file.write(f"<a href=\"{before_url}{file_name}\">https://live.tv.cloudns.be{before_url}{file_name}</a></br>")
    except IOError as e:
        print(f"File error: {e}")

def update_index_html_live(file_name, file_path):
    try:
        with open("./public/box/index.html", "a", encoding='utf-8') as index_file:
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            before_url = '/'
            index_file.write(f"<p><a href=\"{before_url}{file_name}\">https://live.tv.cloudns.be{before_url}{file_name}</a></br>")
    except IOError as e:
        print(f"File error: {e}")

urls_to_fetch = {
    # ... Add other URLs and the strings to replace here ...
    'https://xn--i8sz35d1yr.top/%E7%A6%81%E6%AD%A2%E8%B4%A9%E5%8D%96': ('盒子迷', 'box-mi'),
    'https://xn--i8sz35d1yr.top/4K': ('盒子迷', 'boxmi-4k'),
    'https://xn--i8sz35d1yr.top/%E6%98%A5%E7%9B%88%E5%A4%A9%E4%B8%8B': ('盒子迷', 'cytx_live'),
    'https://hezi.tvboxhz.top/dxaw0': ('盒子迷', 'dxaw0'),
    'https://agit.ai/cyl518/yl/raw/branch/master/ml.json': ('盒子迷', 'pangya'),
    'https://tvbox.meitufu.com/TVBox/tv.json': ('盒子迷', 'taiyang'),
    'http://xn--5mq63w.tvbox.xn--fiqs8s/': ('TVBox', 'vge_multi'),
    'https://tv.youdu.fan:666': ('有毒科技', 'youdu_multi'),
    'http://tv.tv520.shop': ('tv520', 'tv520_multi'),

}

json_id = 0

for url, (old_string, json_name) in urls_to_fetch.items():
    json_id += 1
    new_string = '骚微分享'
    content = fetch_and_replace(url, old_string, new_string)
    
    if content.startswith("Request error:") or content.startswith("Regex error:"):
        print(content)  # Print the error message
        continue

    file_name = f"{json_id}_{json_name}.json"
    file_path = f"./public/box/{file_name}"
    
    write_to_file(file_path, content)
    update_index_html(file_name, "tvbox俊版、影视tv、影视仓等tvbox类可用")

update_index_html("gao/0707.json", "tvbox俊版、影视tv、影视仓等tvbox类")
update_index_html("dy/namegen.json", "tvbox俊版、影视tv、影视仓等tvbox类")


#直播线路_txt

urls_to_fetch = {
    'https://m3u.ibert.me/txt/fmml_ipv6.txt': ('盒子迷', 'fmm_ipv6', '.txt'),
    'https://m3u.ibert.me/txt/j_iptv.txt': ('盒子迷', 'joevess_IPTV', '.txt'),
    'https://m3u.ibert.me/txt/ycl_iptv.txt': ('盒子迷', 'yuechan', '.txt'),
    'https://m3u.ibert.me/txt/o_cn.txt': ('盒子迷', 'iptv.org_China', '.txt'),
    'https://m3u.ibert.me/txt/o_s_cn.txt': ('盒子迷', 'iptv.org_streamChina', '.txt'),
    'https://m3u.ibert.me/txt/cn.txt': ('盒子迷', 'epg.pw_China', '.txt'),
}

for url, (old_string, json_name,suffix) in urls_to_fetch.items():
    new_string = '骚微分享'
    content = fetch_and_replace(url, old_string, new_string)

    if content.startswith("Request error:") or content.startswith("Regex error:"):
        print(content)  # Print the error message
        continue

    file_name = f"{json_name}{suffix}" 
    file_path = f"./public/{file_name}"
    
    write_to_file(file_path, content)
    update_index_html_live(file_name, "txt直播源,tvbox、电视佳可用")



#直播线路_m3u_Json

urls_to_fetch = {
    'https://m3u.ibert.me/fmml_ipv6.m3u': ('盒子迷', 'fmm_ipv6', '.m3u'),
    'https://m3u.ibert.me/j_iptv.m3u': ('盒子迷', 'joevess', '.m3u'),
    'https://m3u.ibert.me/ycl_iptv.m3u': ('盒子迷', 'yuechan', '.m3u'),
    'https://m3u.ibert.me/o_cn.m3u': ('盒子迷', 'iptv.org_China', '.m3u'),
    'https://m3u.ibert.me/o_s_cn.m3u': ('盒子迷', 'iptv.org_streamChina', '.m3u'),
    'https://m3u.ibert.me/cn.m3u': ('盒子迷', 'epg.pw_China', '.m3u'),
    'https://live.tv.cloudns.be/4k_tvbox_cn_noindex.m3u': ('盒子迷', '4ktvbox_cn', '.m3u'),
    'https://live.tv.cloudns.be/zb_tvbox_cn_noindex.txt': ('盒子迷', 'zb_tvbox_cn', '.txt'),
    'https://live.tv.cloudns.be/androidGe_noindex.txt': ('盒子迷', 'androidGe', '.txt'),
    'http://tt.iitvba.com/tv.txt': ('盒子迷', 'tv520', '.txt'),
}

for url, (old_string, json_name, suffix) in urls_to_fetch.items():
    new_string = '骚微分享'
    content = fetch_and_replace(url, old_string, new_string)
        
    if content.startswith("Request error:") or content.startswith("Regex error:"):
        print(content)  # Print the error message
        continue

    file_name = f"{json_name}{suffix}" 
    file_path = f"./public/{file_name}"
    
    write_to_file(file_path, content)
    update_index_html_live(file_name, "m3u直播源,tivimate、电脑也可用")

    mylive = 'https://live.tv.cloudns.be/' + file_name
    content_livejson = fetch_and_replace('https://gitlab.com/orztv/tvbox/-/raw/main/settings/tvbox_jun.json', 'txt直播源填写处', mylive)

    if content_livejson.startswith("Request error:") or content.startswith("Regex error:"):
        print(content)  # Print the error message
        continue

    file_name = f"{json_name}.json"
    file_path = f"./public/box/{file_name}"
    
    write_to_file(file_path, content_livejson)
    update_index_html(file_name, "tvbox直播配置源-只有直播无点播")